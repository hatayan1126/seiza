# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sqlite3
import sys
import math
import codecs

# NumPy:
# https://numpy.org/
# pip install numpy
import numpy as np

# Matplotlib:
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' ログ出力する処理 '''
def write_log(log_file, message):
    #print('[info]: def write_log(log_file, message) が実行されました。')

    with codecs.open(log_file, 'a', 'utf-8') as log:
        log.write(message + '\n')


''' 指定された星座がデータベースに存在するかをチェックする処理 '''
def findCon(dbname, seizaName):
    #print('[info]: def findCon(dbname, seizaName) が実行されました。')

    # SQLiteデータベースに接続
    con = sqlite3.connect(dbname)
    cursor = con.cursor()
    
    # 星座を構成する星を格納するリスト変数 seiza_array を宣言
    seiza_array = []

    # 星座を構成する星を取得するSQLを実行
    s_sql = 'SELECT id FROM hygdata_v3 WHERE con LIKE ? LIMIT 1'
    s_data = (seizaName, )
    s_cursor = cursor.execute(s_sql, s_data)

    # SQLの実行結果をリスト変数 seiza_array に追加
    for s in s_cursor:
        seiza_array.append(s)

    # sqlite クローズ
    con.close()
    
    # 引数で指定された星座の存在チェック
    # 星座が存在していれば True を返す
    if len(seiza_array) > 0:
        return True
    else:
        return False


''' 指定された星座の名前を返す処理 '''
def getCon(dbname, seizaName):
    #print('[info]: def getCon(dbname, seizaName) が実行されました。')

    # SQLiteデータベースに接続
    con = sqlite3.connect(dbname)
    cursor = con.cursor()
    
    # 星座を構成する星を格納するリスト変数 seiza_array を宣言
    seiza_array = []

    # 星座を構成する星を取得するSQLを実行
    s_sql = 'SELECT con FROM hygdata_v3 WHERE con LIKE ? LIMIT 1'
    s_data = (seizaName, )
    s_cursor = cursor.execute(s_sql, s_data)

    # SQLの実行結果をリスト変数 seiza_array に追加
    for s in s_cursor:
        seiza_array.append(s)

    # sqlite クローズ
    con.close()
    
    # 星座名を返す
    return seiza_array[0][0]


''' 指定された星座の赤経が 0時/24時 をまたいでいるかをチェックする処理 '''
def check_s_ra_24cross(dbname, seizaName, seizaLimit):
    #print('[info]: def check_s_ra_24cross(dbname, seizaName, seizaLimit) が実行されました。')
    
    # SQLiteデータベースに接続
    con = sqlite3.connect(dbname)
    cursor = con.cursor()

    # 星座を構成する星を取得するSQLを実行
    s_sql = 'SELECT max(ra)-min(ra) FROM hygdata_v3 WHERE con=? ORDER BY mag LIMIT ?'
    s_data = (seizaName, seizaLimit)
    s_cursor = cursor.execute(s_sql, s_data)

    # 赤経 0時/24時 をまたいでいるか判定
    # 赤経の最大と最小の差が12を超えている場合は 0時/24時 をまたいでいると判定
    # 赤経 0時/24時 をまたいでいれば True を返す
    # ただし、みずがめ座(Aqr)は24時の近くにあるため check_s_ra_24cross() は True とする
    if seizaName == 'Aqr' or seizaName == 'Autumn_Square':
        return True
    else:
        for s in s_cursor:
            #print('[info]: 赤経の最大と最小の差='+str(s[0]))
            if float(s[0]) > 12:
                return True
            else:
                return False

    # sqlite クローズ
    con.close()


''' 星座データを取得する処理 '''
def get_seiza_array(dbname, seizaName, seizaLimit):
    #print('[info]: def get_seiza_array(dbname, seizaName, seizaLimit) が実行されました。')
    # SQLiteデータベースに接続
    con = sqlite3.connect(dbname)
    cursor = con.cursor()

    # 星座を構成する星を格納するリスト変数 seiza_array を宣言
    seiza_array = []

    # 星座を構成する星を取得するSQLを実行
    s_sql = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE con=? ORDER BY mag LIMIT ?'
    s_data = (seizaName, seizaLimit)
    s_cursor = cursor.execute(s_sql, s_data)

    # SQLの実行結果をリスト変数 seiza_array に追加
    for s in s_cursor:
        seiza_array.append(s)

    #print('len(seiza_array)= '+str(len(seiza_array))+' 件')

    # sqlite クローズ
    con.close()

    return seiza_array


''' 恒星データを取得する処理 '''
def get_hoshi_array(dbname, hoshiLimit, hoshimagLimit, s_plot_area, plot_s_ra_24cross):
    #print('[info]: def get_hoshi_array(dbname, hoshiLimit, s_plot_area, plot_s_ra_24cross) が実行されました。')

    # SQLiteデータベースに接続
    # SQLで取得したデータを視等級(mag)でソートするためにコネクションオブジェクトの row_factory に sqlite3.Row を設定する
    con = sqlite3.connect(dbname)
    con.row_factory = sqlite3.Row
    cursor = con.cursor()

    # 指定範囲にある恒星を格納するリスト変数 hoshi_array と hoshi_array_temp を宣言
    hoshi_array = []
    hoshi_array_temp = []

    # 辞書型変数 s_plot{} から値を取り出す
    s_ra_min = s_plot_area['s_ra_min']
    s_ra_max = s_plot_area['s_ra_max']
    s_ra_pad = s_plot_area['s_ra_pad']
    s_dec_min = s_plot_area['s_dec_min']
    s_dec_max = s_plot_area['s_dec_max']
    s_dec_pad = s_plot_area['s_dec_pad']

    if plot_s_ra_24cross == True:
        # 0時側の星座を取得するSQLを実行
        h_sql1 = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?) AND mag < ? ORDER BY mag'
        h_data1 = (0, s_ra_max-24+s_ra_pad, s_dec_min-s_dec_pad, s_dec_max+s_dec_pad, hoshimagLimit)
        #print('h_data1='+str(h_data1))
        h_cursor1 = cursor.execute(h_sql1, h_data1)
        
        # SQLの実行結果をリスト変数 hoshi_array に追加
        for h1 in h_cursor1:
            hoshi_array_temp.append(h1)
            
        # 24時側の星座を取得するSQLを実行
        h_sql2 = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?) AND mag < ? ORDER BY mag'
        h_data2 = (s_ra_min-s_ra_pad, 24, s_dec_min-s_dec_pad, s_dec_max+s_dec_pad, hoshimagLimit)
        #print('h_data2='+str(h_data2))
        h_cursor2 = cursor.execute(h_sql2, h_data2)
        
        # SQLの実行結果をリスト変数 hoshi_array に追加
        for h2 in h_cursor2:
            hoshi_array_temp.append(h2)

        hoshi_array = sorted(hoshi_array_temp, key=lambda x: x['mag'])[:hoshiLimit]

    else:
        # SQLを実行
        h_sql1 = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?) AND mag < ? ORDER BY mag'
        h_data1 = (s_ra_min-s_ra_pad, s_ra_max+s_ra_pad, s_dec_min-s_dec_pad, s_dec_max+s_dec_pad, hoshimagLimit)
        #print('h_data1='+str(h_data1))
        h_cursor1 = cursor.execute(h_sql1, h_data1)
        
        # SQLの実行結果をリスト変数 hoshi_array に追加
        for h1 in h_cursor1:
            hoshi_array_temp.append(h1)

        hoshi_array = sorted(hoshi_array_temp, key=lambda x: x['mag'])[:hoshiLimit]

    #print('len(hoshi_array_temp)='+str(len(hoshi_array_temp))+' 件')
    #print('len(hoshi_array)='+str(len(hoshi_array))+' 件')

    # sqlite クローズ
    con.close()

    return hoshi_array


''' 星座データを取得し描画範囲を算出する処理 '''
def calc_seiza_draw_area(s_ra_float, s_dec_float, s_pad):
    #print('[info]: def calc_seiza_draw_area(s_ra_float, s_dec_float, s_pad) が実行されました。')

    # 辞書型変数 s_plot_area を宣言
    s_plot_area = {}

    # 赤経最小値を変数「s_ra_min」に代入
    s_ra_min = min(s_ra_float)
    #print('s_ra_min='+str(s_ra_min))

    # 赤経最大値を変数「s_ra_max」に代入
    s_ra_max = max(s_ra_float)
    #print('s_ra_max='+str(s_ra_max))

    # 赤経範囲を変数「s_ra_delta」に代入
    s_ra_delta = s_ra_max-s_ra_min
    #print('s_ra_delta='+str(s_ra_delta))

    # 赤経の中心を取得し「s_ra_center」に代入
    s_ra_center = s_ra_min+(s_ra_delta/2)
    #print('s_ra_center='+str(s_ra_center))

    # 赤経パッディングを変数「s_ra_pad」に代入
    s_ra_pad = s_ra_delta*s_pad
    #print('s_ra_pad='+str(s_ra_pad))

    # 赤緯最小値を変数「s_dec_min」に代入
    s_dec_min = min(s_dec_float)
    #print('s_dec_min='+str(s_dec_min))

    # 赤緯最大値を変数「s_dec_max」に代入
    s_dec_max = max(s_dec_float)
    #print('s_dec_max='+str(s_dec_max))

    # 赤緯範囲を変数「s_dec_delta」に代入
    s_dec_delta = s_dec_max-s_dec_min
    #print('s_dec_delta='+str(s_dec_delta))

    # 赤緯の中心を取得し「s_dec_center」に代入
    s_dec_center = s_dec_min+(s_dec_delta/2)
    #print('s_dec_center='+str(s_dec_center))

    # 赤緯パッディングを変数「s_dec_pad」に代入
    s_dec_pad = s_dec_delta*s_pad
    #print('s_dec_pad='+str(s_dec_pad))

    # 赤経と赤緯の長さをチェックして長い方に合わせる
    # 赤経は時分秒(hms)なので赤緯の度分秒と単位を揃えるため x15 する
    if s_ra_delta*15 > s_dec_delta:
        #print('s_ra_deltaが長い')
        s_plot_area['s_ra_min'] = s_ra_min-(s_dec_pad/15)
        s_plot_area['s_ra_max'] = s_ra_max+(s_dec_pad/15)
        s_plot_area['s_ra_pad'] = s_ra_pad
        s_plot_area['s_dec_min'] = s_dec_center-((s_ra_delta*15)/2)-s_dec_pad
        s_plot_area['s_dec_max'] = s_dec_center+((s_ra_delta*15)/2)+s_dec_pad
        s_plot_area['s_dec_pad'] = s_dec_pad
    elif s_ra_delta*15 < s_dec_delta:
        #print('s_dec_deltaが長い')
        s_plot_area['s_ra_min'] = s_ra_center-((s_dec_delta/15)/2)-s_ra_pad
        s_plot_area['s_ra_max'] = s_ra_center+((s_dec_delta/15)/2)+s_ra_pad
        s_plot_area['s_ra_pad'] = s_ra_pad
        s_plot_area['s_dec_min'] = s_dec_min-(s_ra_pad*15)
        s_plot_area['s_dec_max'] = s_dec_max+(s_ra_pad*15)
        s_plot_area['s_dec_pad'] = s_dec_pad

    #print('s_plot_area='+str(s_plot_area))

    return s_plot_area


''' 恒星データを取得し描画範囲を算出する処理 '''
def calc_hoshi_draw_area(h_ra_float, h_dec_float):
    #print('[info]: def calc_hoshi_draw_area(h_ra_float, h_dec_float) が実行されました。')

    # 辞書型変数 h_plot_area を宣言
    h_plot_area = {}

    # 赤経最小値を変数「h_ra_min」に代入
    h_plot_area['h_ra_min'] = min(h_ra_float)
    #print('h_ra_min='+str(s_plot_area['h_ra_min']))

    # 赤経最大値を変数「h_ra_max」に代入
    h_plot_area['h_ra_max'] = max(h_ra_float)
    #print('h_ra_max='+str(s_plot_area['h_ra_max']))

    # 赤緯最小値を変数「h_dec_min」に代入
    h_plot_area['h_dec_min'] = min(h_dec_float)
    #print('h_dec_min='+str(s_plot_area['h_dec_min']))

    # 赤緯最大値を変数「h_dec_max」に代入
    h_plot_area['h_dec_max'] = max(h_dec_float)
    #print('h_dec_max='+str(s_plot_area['h_dec_max']))

    #print('h_plot_area='+str(h_plot_area))

    return h_plot_area


''' 恒星データから中心を算出する処理 '''
def calc_draw_area_center(h_ra_max, h_ra_min):
    #print('[info]: def calc_draw_area_center(h_ra_max, h_ra_min) が実行されました。')

    draw_area_center = h_ra_max-(h_ra_max-h_ra_min)/2

    return draw_area_center


''' 星座をプロットする処理 '''
def plot_s_star(s_ra, s_dec, s_ma, msCorrection, s_pr, hoshiColor):
    #print('[info]: def plot_s_star(s_ra, s_dec, s_ma, msCorrection, s_pr, hoshiColor) が実行されました。')

    flag_plot_s_star = False

    if hoshiColor == 'mono':
        if math.floor(s_ma) <= 1:
            m_size = 2.00
        elif math.floor(s_ma) == 2:
            m_size = 1.75
        elif math.floor(s_ma) == 3:
            m_size = 1.50
        elif math.floor(s_ma) == 4:
            m_size = 1.25
        elif math.floor(s_ma) == 5:
            m_size = 1.00
        else:
            m_size = 0.75

        # マーカープロット
        # c:color, ms:markersize, mew:markeredgewidth
        plt.plot(s_ra, s_dec, marker='.', color='red', markersize=(m_size/0.3528)*msCorrection, markeredgewidth=0.1)
        # マーカー注釈
        plt.annotate(s_pr, xy=(s_ra, s_dec), xytext=(s_ra-0.025, s_dec-0.01), color='red', size=3)

        flag_plot_s_star = True

    elif hoshiColor == 'fullcolor':
        if math.floor(s_ma) <= 1:
            m_size = 1.60
        elif math.floor(s_ma) == 2:
            m_size = 1.40
        elif math.floor(s_ma) == 3:
            m_size = 1.20
        elif math.floor(s_ma) == 4:
            m_size = 1.00
        elif math.floor(s_ma) == 5:
            m_size = 0.80
        elif math.floor(s_ma) == 6:
            m_size = 0.60
        elif math.floor(s_ma) == 7:
            m_size = 0.50
        elif math.floor(s_ma) == 8:
            m_size = 0.40
        else:
            m_size = 0.30 

        # マーカープロット
        # c:color, ms:markersize, mew:markeredgewidth
        plt.plot(s_ra, s_dec, marker='.', color='yellow', markersize=(m_size/0.3528)*msCorrection, markeredgewidth=0.1)
        # マーカー注釈
        plt.annotate(s_pr, xy=(s_ra, s_dec), xytext=(s_ra-0.025, s_dec-0.01), color='white', size=3)

        flag_plot_s_star = True
    
    return flag_plot_s_star


''' 恒星をプロットする処理 '''
def plot_h_star(h_ra, h_dec, h_mag, msCorrection, h_pr, hoshiColor):
    #print('[info]: def plot_h_star(h_ra, h_dec, h_mag, msCorrection, h_pr, hoshiColor) が実行されました。')

    flag_plot_h_star = False

    if hoshiColor == 'mono':
        if math.floor(h_mag) <= 1:
            m_size = 2.00
        elif math.floor(h_mag) == 2:
            m_size = 1.75
        elif math.floor(h_mag) == 3:
            m_size = 1.50
        elif math.floor(h_mag) == 4:
            m_size = 1.25
        elif math.floor(h_mag) == 5:
            m_size = 1.00
        else:
            m_size = 0.75

        # マーカープロット
        # c:color, ms:markersize, mew:markeredgewidth
        plt.plot(h_ra, h_dec, marker='.', color='red', markersize=(m_size/0.3528)*msCorrection, markeredgewidth=0.1)
        # マーカー注釈
        plt.annotate(h_pr, xy=(h_ra,h_dec), xytext=(h_ra-0.025, h_dec-0.01), color='red', size=3)

        flag_plot_h_star = True
        
    elif hoshiColor == 'fullcolor':
        if math.floor(h_mag) <= 1:
            m_size = 1.60
        elif math.floor(h_mag) == 2:
            m_size = 1.40
        elif math.floor(h_mag) == 3:
            m_size = 1.20
        elif math.floor(h_mag) == 4:
            m_size = 1.00
        elif math.floor(h_mag) == 5:
            m_size = 0.80
        elif math.floor(h_mag) == 6:
            m_size = 0.60
        elif math.floor(h_mag) == 7:
            m_size = 0.50
        elif math.floor(h_mag) == 8:
            m_size = 0.40
        else:
            m_size = 0.30 

        # マーカープロット
        # c:color, ms:markersize, mew:markeredgewidth
        plt.plot(h_ra, h_dec, marker='.', color='white', markersize=(m_size/0.3528)*msCorrection, markeredgewidth=0.1)
        # マーカー注釈
        plt.annotate(h_pr, xy=(h_ra,h_dec), xytext=(h_ra-0.025, h_dec-0.01), color='white', size=3)

        flag_plot_h_star = True

    return flag_plot_h_star


''' 恒星間の距離を算出する処理 '''
def calc_h_distance(h1_ra, h1_de, h2_ra, h2_de):
    #print('[info]: def calc_h_distance(h1_ra, h1_de, h2_ra, h2_de) が実行されました。')

    pointx = np.array([h1_ra, h1_de/15])
    pointy = np.array([h2_ra, h2_de/15])
    h_distance = np.linalg.norm(pointx-pointy)
    #print('h_distance='+str(h_distance))

    return h_distance


''' 最寄りの恒星を抽出する処理 '''
def extract_nearest_star(h1_ra, h1_dec, hoshi_array, mag, h2_mag, plot_s_ra_24cross):
    #print('[info]: def extract_nearest_star(h1_ra, h1_dec, hoshi_array, h2_mag, plot_s_ra_24cross) が実行されました。')

    h2_array = np.empty((0,3),float)

    for h2 in hoshi_array:
        h2_mag_temp = h2[2]
        if plot_s_ra_24cross == True:
            if h2[3] < 12:
                h2_ra_temp = h2[3]+24
            else:
                h2_ra_temp = h2[3]
        else:
            h2_ra_temp = h2[3]
        
        h2_dec_temp = h2[4]

        #print('h2_mag_temp='+str(h2_mag_temp))
        #print('h2_ra_temp='+str(h2_ra_temp))
        #print('h2_dec_temp='+str(h2_dec_temp))

        if mag == 0:
            if h2_mag < 1:
                h1_h2_distance = calc_h_distance(h1_ra, h1_dec, h2_ra, h2_dec)
                #print('h1_h2_distance='+str(h1_h2_distance))

                if h1_h2_distance > 0:
                    h2_array = np.append(h2_array, np.array([[h2_ra, h2_dec, h1_h2_distance]]), axis=0)
        else:
            if mag <= h2_mag < mag+1:
                h1_h2_distance = calc_h_distance(h1_ra, h1_dec, h2_ra, h2_dec)
                #print('h1_h2_distance='+str(h1_h2_distance))

                if h1_h2_distance > 0:
                    h2_array = np.append(h2_array, np.array([[h2_ra, h2_dec, h1_h2_distance]]), axis=0)

        #print('h2_array='+str(h2_array))
    
    nearest_index = np.argmin(h2_array, axis=0)[2]
    #print('nearest_index='+str(nearest_index))
    #print('h2_array[nearest_index]='+str(h2_array[nearest_index]))

    h2_ra = h2_array[nearest_index][0]
    h2_dec = h2_array[nearest_index][1]
    h2_position = [h2_ra, h2_dec]

    return h2_position


''' 恒星間の線を描画する処理 '''
def draw_h_line(h1_ra, h1_dec, h2_ra, h2_dec):
    #print('[info]: def draw_h_line(h1_ra, h1_dec, h2_ra, h2_dec) が実行されました。')
    
    plt.plot([h1_ra, h2_ra,], [h1_dec, h2_dec], color="white", linewidth=0.1)
    #print(str(h1_ra)+','+str(h1_dec)+' - '+str(h2_ra)+','+str(h2_dec)+' の線を引きます')


''' 画像データを出力する処理 '''
def output_file(outputDir, outputFilename):
    #print('[info]: def output_file(outputDir, outputFilename) が実行されました。')

    # 余白の調整
    plt.subplots_adjust(left=0.01, right=0.99, bottom=0.01, top=0.99)
    
    # ファイル出力 PDF形式
    #plt.savefig(outputDir + '/' + outputFilename + '.pdf', facecolor='azure')
    #plt.savefig(outputDir + '/' + outputFilename + '.pdf')
    #print('Generated ' + outputDir + '/' + outputFilename + '.pdf' + ' !!')
    
    # ファイル出力 PNG形式
    #plt.savefig(outputDir + '/' + outputFilename + '.png', facecolor='azure')
    #print('Generated ' + outputDir + '/' + outputFilename + '.png' + ' !!')
    
    # ファイル出力 SVG形式
    plt.savefig(outputDir + '/' + outputFilename + '.svg', facecolor='azure')
    print('[info]: ' + outputDir + '/' + outputFilename + '.svg' + ' を生成しました。')
