# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys
import sqlite3

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-sqlite.py', 'Ori', '3',]
#print(args[0])     # make-seiza-sqlite.py
#print(args[1])     # Ori
#print(args[2])     # 3～5

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename = os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaName(星座名)」に代入
seizaName = args[1]
#print(seizaName)

# 第2引数を変数「seizaMag(視等級)」に代入
seizaMag = args[2]
#print(seizaMag)


''' SQLite操作処理 '''

# SQLiteデータベースに接続
dbname = './hygdatav3.db'
conn = sqlite3.connect(dbname)
cursor = conn.cursor()

# SQLを実行
sql = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE con=? AND mag<=?'
data = (seizaName, seizaMag)
cursor.execute(sql, data)

# リスト変数 data_array を宣言
data_array = []

# SQLの実行結果をリスト変数 data_array に追加
for c in cursor:
    data_array.append(c)

# sqlite クローズ
conn.close()


''' データ変換処理 '''

# リスト変数 hip, proper, mag, plot_ra, plot_dec を宣言
hip = []       # HipparcosID番号
proper = []    # 恒星名
mag = []       # 見かけの等級
plot_ra = []   # 赤経
plot_dec = []  # 赤緯

# リスト変数  hip, proper, mag, plot_ra, plot_dec に data_array のデータを追加
for i in data_array:
    hip.append(i[0])
    proper.append(i[1])
    mag.append(i[2])
    plot_ra.append(i[3])
    plot_dec.append(i[4])

# リスト mag を float型 に変換
mag_float = [float(s) for s in mag]
#print(mag_float)

# 赤経リスト plot_ra を float型 に変換
plot_ra_float = [float(s) for s in plot_ra]
# 時分秒(hms)を度分秒に変換する場合は x15 する
#plot_ra_float = [(float(s)*15) for s in plot_ra]
#print(plot_ra_float)

# 赤緯リスト plot_dec を float型 に変換
plot_dec_float = [float(s) for s in plot_dec]
#print(plot_dec_float)


''' 星座描画処理 '''

# 画像ザイズ指定
# 縦横を mm で指定
figX_mm = 100
figY_mm = 148

# pdf出力時はインチ単位なので単位変換
figX_in = round(((figX_mm*1.5)*0.03937), 2)
figY_in = round(((figY_mm*1.5)*0.03937), 2)

# X軸に対するY軸の比率を計算
plt.figure(figsize=(figX_in, figY_in))

# グラフの外枠を非表示
#plt.axis('off')

# 背景とタイトルの設定
#plt.gca(facecolor='white', title=seizaName + ' (Vmag<=' + seizaMag + ') ' + appFilename)

# グラフの縦横比(アスペクト比)
# plot_ra_floatが時分秒(hms)なので、縦横比をequalにするために 1:1/15(0.066...) に設定
plt.gca().set_aspect(1/15)

# x座標を反転させる
plt.gca().invert_xaxis()

# 軸の目盛線表示
plt.gca().tick_params(which='both', direction='inout', color='black')
plt.gca().tick_params(bottom=True, left=True, right=True, top=True)

# 軸の目盛ラベル表示
plt.gca().tick_params(labelbottom=False, labelleft=False, labelright=False, labeltop=False)

# 補助線表示設定
plt.gca().grid(which='major', linestyle='-', linewidth=0.1, color='black')

# 座標をプロット
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.plot.html
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.annotate.html

for (hi, pr, ma, ra, de) in zip(hip, proper, mag_float, plot_ra_float, plot_dec_float):
    #print(hi)
    #print(pr)
    #print(ma)
    #print(ra)
    #print(de)
    # 視等級に応じてマーカーの形、色(グレースケール)、サイズ を変える
    if round(ma, 0) <= 0:
        plt.plot(ra, de, marker='o', color='black', markersize=3.0)
    elif round(ma, 0) == 1:
        plt.plot(ra, de, marker='o', color='black', markersize=2.5)
    elif round(ma, 0) == 2:
        plt.plot(ra, de, marker='o', color='black', markersize=2.0)
    elif round(ma, 0) == 3:
        plt.plot(ra, de, marker='o', color='black', markersize=1.5)
    elif round(ma, 0) == 4:
        plt.plot(ra, de, marker='o', color='black', markersize=1.0)
    elif round(ma, 0) == 5:
        plt.plot(ra, de, marker='o', color='black', markersize=0.5)
    elif round(ma, 0) >= 6:
        plt.plot(ra, de, marker='o', color='black', markersize=0.25)
        
    # マーカー注釈
    plt.annotate(pr, xy=(ra, de), xytext=(ra-0.01, de-0.01), size=4)

''' ファイル出力処理 '''

# ファイル名
outputFilename = appFilename + '_' + seizaName + '_' + seizaMag

# 余白設定 上下左右均等
pad_mm = 3
pad_in = round((float(pad_mm)*0.03937), 2)
#print(pad_in)

# ファイル出力 PDF形式
plt.savefig('./' + outputFilename + '.pdf', facecolor='white', bbox_inches='tight', pad_inches=pad_in)
print('Generated ' + outputFilename + '.pdf' + ' !!')

# ファイル出力 PNG形式
#plt.savefig('./' + outputFilename + '.png', facecolor='azure', bbox_inches='tight', pad_inches=pad_in)
#print('Generated ' + outputFilename + '.png' + ' !!')

# ファイル出力 SVG形式
#plt.savefig('./' + outputFilename + '.svg', facecolor='azure', bbox_inches='tight', pad_inches=pad_in)
#print('Generated ' + outputFilename + '.svg' + ' !!')

