# Standard Library
# https://docs.python.org/ja/3/library/index.html
import configparser
import os
import os.path
import sqlite3
import sys

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-sqlite.py', '3']
#print(args[0])     # make-seiza-sqlite.py
#print(args[1])     # 視等級

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename = os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「starMag(視等級)」に代入
starMag = args[1]
#print(starMag)


''' iniファイル読み込み '''

ini = configparser.ConfigParser()
ini.read('./make-hoshizora-sqlite.ini', 'UTF-8')

# 横サイズを変数「figX_mm」に代入
figX_mm = ini['config']['figX_mm']
#print(figX_mm)

# 横サイズをinに変換して変数「figX_in」に代入
figX_in = round((float(figX_mm)*0.03937), 2)
#print(figX_in)

# 縦サイズを変数「figY_mm」に代入
figY_mm = ini['config']['figY_mm']
#print(figY_mm)

# 縦サイズをinに変換して変数「figX_in」に代入
figY_in = round((float(figY_mm)*0.03937), 2)
#print(figY_in)


# 赤経最小値を変数「plot_ra_min」に代入
plot_ra_min = float(ini['config']['plot_ra_min'])
#print(plot_ra_min)

# 赤経最大値を変数「plot_ra_max」に代入
plot_ra_max = float(ini['config']['plot_ra_max'])
#print(plot_ra_max)

# 赤緯最小値を変数「plot_dec_min」に代入
plot_dec_min = float(ini['config']['plot_dec_min'])
#print(plot_dec_min)

# 赤経最大値を変数「plot_dec_max」に代入
plot_dec_max = float(ini['config']['plot_dec_max'])
#print(plot_dec_max)


''' SQLite操作処理 '''

# SQLiteデータベースに接続
dbname = './make-seiza-sqlite-hygdatav3.db'
conn = sqlite3.connect(dbname)
cursor = conn.cursor()

# リスト変数 data_array を宣言
data_array = []

for m in range(int(starMag) + 1):
    #print(m)
    
    starMag_min = float(m)
    #print(starMag_min)
    
    starMag_max = float(m) + 0.99
    #print(starMag_max)
    
    # リスト変数をクリアする
    data_array.clear()
    
    # SQLを実行
    sql = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE (mag BETWEEN ? AND ?) AND (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?)'
    data = (starMag_min, starMag_max, plot_ra_min, plot_ra_max, plot_dec_min, plot_dec_max)
    cursor.execute(sql, data)
    
    # SQLの実行結果をリスト変数 data_array に追加
    for c in cursor:
        data_array.append(c)
    
    # レコードカウンター
    star_count = len(data_array)
    #print(str(m) + '=' + str(star_count))
    
    ''' データ変換処理 '''
    
    # リスト変数 hip, proper, mag, plot_ra, plot_dec を宣言
    hip = []       # HipparcosID番号
    proper = []    # 恒星名
    mag = []       # 視等級
    plot_ra = []   # 赤経
    plot_dec = []  # 赤緯
    
    # リスト変数  hip, proper, mag, plot_ra, plot_dec に data_array のデータを追加
    for i in data_array:
        hip.append(i[0])
        proper.append(i[1])
        mag.append(i[2])
        plot_ra.append(i[3])
        plot_dec.append(i[4])
    
    #print(hip)
    #print(proper)
    #print(mag)
    #print(plot_ra)
    #print(plot_dec)
    
    # リスト mag を float型 に変換
    mag_float = [float(s) for s in mag]
    #print(mag_float)
    
    # mag_float を マーカーサイズ用 mag_float_s に変換
    # 第2引数で指定した視等級を基準に調整
    mag_float_s = [round(((float(starMag) + 3) - (float(s) + 2)) * 0.25, 2) for s in mag]
    #print(mag_float_s)
    
    # 赤経リスト plot_ra を float型 に変換
    plot_ra_float = [float(s) for s in plot_ra]
    # 時分秒(hms)を度分秒に変換する場合は x15 する
    #plot_ra_float = [(float(s)*15) for s in plot_ra]
    #print(plot_ra_float)
    
    # 赤緯リスト plot_dec を float型 に変換
    plot_dec_float = [float(s) for s in plot_dec]
    #print(plot_dec_float)
    
    
    ''' 星座描画処理 '''
    
    # 画像ザイズ指定
    # pdf出力時はインチ、png出力時はピクセル
    plt.figure(figsize=(figX_in, figY_in))
    
    # グラフの外枠を非表示
    plt.axis('off')
    
    # 背景とタイトルの設定
    #plt.gca(facecolor='white', title='hoshizora' + '/ Vmag:' + str(m) + ' / ' + str(star_count) + ' stars')
    
    # グラフの縦横比(アスペクト比)
    # plot_ra_floatが時分秒(hms)なので、縦横比をequalにするために 1:1/15(0.066...) に設定
    plt.gca().set_aspect(1/15)
    
    # x軸の範囲設定
    plt.gca().set_xlim(plot_ra_min, plot_ra_max)
    
    # y軸の範囲設定
    plt.gca().set_ylim(plot_dec_min, plot_dec_max)
    
    # x座標を反転させる
    plt.gca().invert_xaxis()
    
    # 軸の目盛ラベル設定
    plt.gca().tick_params(which='both', direction='in', labelsize=4, pad=-12)
    
    # 軸の目盛ラベル非表示
    #plt.gca().tick_params(labelbottom=False, labelleft=False, labelright=False, labeltop=False)
    
    # 軸の目盛線非表示
    #plt.gca().tick_params(bottom=False, left=False, right=False, top=False)
    
    # 軸ラベルの向きを変える
    #plt.gca().yaxis.set_tick_params(labelrotation=90)
    
    # 軸の補助目盛表示設定
    # plot_ra_floatが時分秒(hms)なので、X軸の補助目盛の間隔は10分(10/60分)
    #plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(10/60))
    # Y軸の補助目盛 1度
    #plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(1))
    
    # 補助線表示設定
    #plt.gca().grid(which='major', linestyle='-', linewidth=0.1)
    #plt.gca().grid(which='minor', linestyle=':', linewidth=0.05)
    
    # 座標をプロット
    # https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.plot.html
    # https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.annotate.html
    
    for (hi, pr, ma, ms, ra, de) in zip(hip, proper, mag_float, mag_float_s, plot_ra_float, plot_dec_float):
        #print(hi)
        #print(pr)
        #print(ma)
        #print(ms)
        #print(ra)
        #print(de)
        # 視等級に応じてマーカーの形、色(グレースケール)、サイズ を変える
        plt.plot(ra, de, marker='o', color='k', markersize=ms)
            
        # マーカー注釈
        plt.annotate(pr, xy=(ra, de), xytext=(ra - 0.025, de - 0.01), size=3)
        
    plt.plot(plot_ra_min, plot_dec_min, marker='.', color='k')
    plt.plot(plot_ra_max, plot_dec_min, marker='.', color='k')
    plt.plot(plot_ra_min, plot_dec_max, marker='.', color='k')
    plt.plot(plot_ra_max, plot_dec_max, marker='.', color='k')
    
    ''' ファイル出力処理 '''
    
    # ファイル名
    outputFilename = appFilename + '_' + str(m).zfill(2)
    
    # 余白設定 上下左右均等
    pad_mm = 5
    pad_in = round((float(pad_mm) * 0.03937), 2)
    #print(pad_in)
    
    # ファイル出力 PDF形式
    plt.savefig('./' + outputFilename + '.pdf', facecolor='white', bbox_inches='tight', pad_inches=pad_in)
    print('Generated ' + outputFilename + '.pdf' + ' !!')
    
    # ファイル出力 PNG形式
    #plt.savefig('./' + outputFilename + '.png', facecolor='azure', bbox_inches='tight', pad_inches=pad_in)
    #print('Generated ' + outputFilename + '.png' + ' !!')
    
    # ファイル出力 SVG形式
    #plt.savefig('./' + outputFilename + '.svg', facecolor='azure', bbox_inches='tight', pad_inches=pad_in)
    #print('Generated ' + outputFilename + '.svg' + ' !!')
    

# sqlite クローズ
conn.close()

