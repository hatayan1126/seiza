'''
https://qiita.com/phyblas/items/a801b0f319742245ad2e
pip install astroquery

--------------- ---------
Package         Version
--------------- ---------
astropy         3.2.1
astroquery      0.3.9
beautifulsoup4  4.8.0
certifi         2019.6.16
chardet         3.0.4
cycler          0.10.0
entrypoints     0.3
html5lib        1.0.1
idna            2.8
keyring         19.0.2
kiwisolver      1.1.0
matplotlib      3.1.1
numpy           1.17.0
pip             19.2.2
pyparsing       2.4.2
python-dateutil 2.8.0
pywin32-ctypes  0.2.0
requests        2.22.0
setuptools      41.1.0
six             1.12.0
soupsieve       1.9.2
urllib3         1.25.3
webencodings    0.5.1
wheel           0.33.4
--------------- ---------

'''

import os

import astropy.coordinates
from astroquery.simbad import Simbad

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


simbad = Simbad()
simbad.add_votable_fields('flux(V)')
hoshi = simbad.query_criteria('Vmag<5',otype='star')
print(hoshi)
