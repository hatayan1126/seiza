# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys
import csv

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-csv.py', 'Orion', '3', 'pdf']
#print(args[0])     # make-seiza-csv.py
#print(args[1])     # Orion
#print(args[2])     # 3～5
#print(args[3])     # pdf or png

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename=os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaName(星座名)」に代入
seizaName=args[1]
#print(seizaName)

# 第2引数を変数「seizaMag(視等級)」に代入
seizaMag=args[2]
#print(seizaMag)

# 第3引数を変数「fileType(ファイル形式)」に代入
fileType=args[3]
#print(fileType)

''' CSVファイル操作処理 '''

# CSVファイルを読み込んでリスト変数 data_array に代入
with open('./make-seiza-' + seizaName + '_sjis.csv', encoding='sjis') as csvfile:
    reader = csv.reader(csvfile)
    
    header = next(reader)
    #print(",".join(header))
    
    data_array = [row for row in reader]
    #print(data_array)


''' データ変換処理 '''

# リスト変数 hip, proper, mag, plot_ra, plot_dec を宣言
hip = []       # HipparcosID番号
proper = []    # 恒星名
mag = []       # 見かけの等級
plot_ra = []   # 赤経
plot_dec = []  # 赤緯

# リスト変数 hip, mag, plot_ra, plot_dec に data_array のデータを追加
for i in data_array:
    if float(i[6]) > float(seizaMag):
        continue
    hip.append(i[0])
    proper.append(i[4])
    mag.append(i[6])
    plot_ra.append(i[7])
    plot_dec.append(i[8])

# リスト mag を float型 に変換
mag_float = [float(s) for s in mag]
#print(mag_float)

# mag_float を マーカーサイズ用 mag_float_s に変換
mag_float_s = [round(25/float(s)) for s in mag]
#print(mag_float_s)

# リスト plot_ra を float型 に変換
plot_ra_float = [(float(s)*15) for s in plot_ra]
#print(plot_ra_float)

# リスト plot_dec を float型 に変換
plot_dec_float = [float(s) for s in plot_dec]
#print(plot_dec_float)


''' 星座描画処理 '''

# 画像ザイズ指定

if fileType=='pdf':
    #print(fileType)
    
    # pdf出力時：インチ指定
    plt.figure(figsize=(3.94, 5.83)) # はがき タテ
    
elif fileType=='png':
    
    # png出力時：ピクセル指定
    # はがき 低解像度 96dpi 約33KB
    plt.figure(figsize=(378/100, 559/100), dpi=96)
    
    # 赤緯180度 : 赤経360度 = 1:2
    #plt.figure(figsize=(180/100, 360/100), dpi=96)
    
else:
    print('拡張子は pdf か png を指定してください。')
    exit()

# 背景とタイトルの設定
plt.gca(facecolor='white', title=seizaName + ' (Vmag<=' + seizaMag + ')')

# x座標を反転させる
plt.gca().invert_xaxis()

# 軸の目盛ラベル設定
plt.gca().tick_params(labelsize=5, which='both', direction='inout')

# 軸ラベルの向きを変える
plt.gca().yaxis.set_tick_params(labelrotation=90)

# 軸の補助目盛表示設定
plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(0.5))
plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(1))

# 補助線表示設定
plt.gca().grid(which='both', linestyle = ':', linewidth = 0.5)

# 座標をプロット
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.plot.html
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.annotate.html

for (h,p,m,r,d) in zip(hip, proper, mag_float, plot_ra_float, plot_dec_float):
    #print(r)
    #print(d)
    #print(h)
    #print(p)
    plt.plot(r,d, marker='*', color='blue', markersize=3)
    plt.annotate(p+'('+str(m)+')', xy=(r, d), xytext=(r-0.35, d-0.1), size=5)

# ファイルに出力
outputFilename=appFilename + '_' + seizaName + '(' + seizaMag + ').' + fileType
plt.savefig('./' + outputFilename, facecolor="azure", bbox_inches='tight', pad_inches=0.1)
print('Generated ' + outputFilename + ' !!')
