# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys
import warnings
warnings.filterwarnings('ignore')

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# astropy
# https://www.astropy.org/
# pip install astropy
# pip install astroquery
import astropy.coordinates
from astropy.coordinates import SkyCoord
from astroquery.simbad import Simbad

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-astropy.py', 'Ori', '3', 'pdf']
#print(args[0])     # make-seiza-astropy.py
#print(args[1])     # Ori
#print(args[2])     # ～4.5
#print(args[3])     # 100mm(1mm=0.03937in)

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename=os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaName(星座名)」に代入
seizaName = args[1]
#print(seizaName)

# 第2引数を変数「seizaMag(視等級)」に代入
seizaMag = args[2]
#print(seizaMag)

if float(seizaMag)>4.5:
    print('<<!ERROR!>>視等級は4.5以下 で指定してください')
    exit()

# 第3引数をmmを変数「figSize_mm」に代入
figSize_mm = args[3]
#print(figSize_mm)

# 第3引数をmmをinに変換して変数「figSize_in」に代入
figSize_in = round((float(figSize_mm)*0.03937),2)
#print(figSize_in)


''' Simbadデータベース操作処理 '''

# ---------------------------
# SimbadClass
# https://astroquery.readthedocs.io/en/latest/api/astroquery.simbad.SimbadClass.html
# ---------------------------
#
# クラス「Simbad()」のインスタンス「simbad」を生成
simbad = Simbad()

# 項目に視等級「flux(V)」を追加
simbad.add_votable_fields('flux(V)')

# 変数「hoshi」に simbad.query_criteria の結果を代入
# フィルタ：視等級(Vmag)を第2引数(seizaMag)以下で、タイプは星(star)のみとする
hoshi = simbad.query_criteria('Vmag<=' + seizaMag, otype='star')
#print(hoshi[0])
#print('hoshi[]の要素数：' + str(len(hoshi)))


''' データ変換処理 '''

# リスト変数 mag, plot_ra, plot_dec を宣言
mag_float = []      # 見かけの等級
plot_ra_float = []  # 赤経
plot_dec_float = [] # 赤緯


# SkyCoordで座標データの変換
# RA  'h:m:s'  02 03 53.9531 → deg 30d58m29.2965s
# DEC 'd:m:s' +42 19 47.009  → deg 42d19m47.009s
# ---------------------------
# SkyCoord
# https://docs.astropy.org/en/stable/api/astropy.coordinates.SkyCoord.html
# ---------------------------
sc = SkyCoord(ra=hoshi['RA'], dec=hoshi['DEC'], unit=['hourangle', 'deg'])
#print(sc)

# 星座名を取得
seiza = sc.get_constellation(short_name='true')
#print(seiza)

# 第1引数(seizaName)に一致していれば True、そうでなけれなFalse
# 以降 [o] はフィルタとして機能する
o = (seiza==seizaName)
#print(o)

# 視等級 hoshi['FLUX_V'] を mag[] に追加
for i in hoshi['FLUX_V'][o]:
    if str(i) == '--':
        continue
    mag_float.append(i)
#print(mag_float)

# mag_float を マーカーサイズ用 mag_float_s に変換
# 第2引数で指定した視等級を基準に調整
mag_float_s = [round(((float(seizaMag)+3)-(s+2))*0.5, 2) for s in mag_float]
#print(mag_float_s)

# 赤経(right ascension)
# sc.ra が度分秒(dms)なので /15 で時分秒に変換
for i in sc.ra[o].deg/15:
    plot_ra_float.append(i)
#print(plot_ra_float)

# 赤経の最大値
plot_ra_max_float = max(plot_ra_float)
#print(plot_ra_max_float)

# 赤経の最小値
plot_ra_min_float = min(plot_ra_float)
#print(plot_ra_min_float)

# 赤経の差＝X軸の範囲
# 単位を揃える：時分秒(hms)を度分秒に変換するため x15 する
plot_ra_delta_float = (plot_ra_max_float-plot_ra_min_float)*15
#print(plot_ra_delta_float)

# 赤緯(declination)
for i in sc.dec[o].deg:
    plot_dec_float.append(i)
#print(plot_dec_float)

# 赤緯の最大値
plot_dec_max_float = max(plot_dec_float)
#print(plot_dec_max_float)

# 赤緯の最小値
plot_dec_min_float = min(plot_dec_float)
#print(plot_dec_min_float)

# 赤緯の差＝Y軸の範囲
plot_dec_delta_float = plot_dec_max_float - plot_dec_min_float
#print(plot_dec_delta_float)


''' 星座描画処理 '''

# 画像ザイズ指定
# pdf出力時はインチ、png出力時はピクセル

# X軸に対するY軸の比率を計算
plot_aspect_float = plot_dec_delta_float/plot_ra_delta_float
#print(plot_aspect_float)

# グラフサイズの横は第2引数(figSize_mm)で固定
# グラフサイズの縦はX軸とY軸の比に応じて可変
plt.figure(figsize=(figSize_in, figSize_in*plot_aspect_float))

# グラフの外枠を非表示
#plt.axis('off')

# 背景とタイトルの設定
plt.gca(facecolor='white', title=seizaName + ' (Vmag<=' + seizaMag + ') ' + appFilename)

# グラフの縦横比(アスペクト比)
# plot_ra_floatが時分秒(hms)なので、縦横比をequalにするために 1:1/15(0.066...) に設定
plt.gca().set_aspect(1/15)

# x座標を反転させる
plt.gca().invert_xaxis()

# 軸の目盛ラベル設定
plt.gca().tick_params(which='both', direction='inout', labelsize=4, pad=-10)

# 軸の目盛ラベル非表示
#plt.gca().tick_params(labelbottom=False, labelleft=False, labelright=False, labeltop=False)
# 軸の目盛線非表示
#plt.gca().tick_params(bottom=False, left=False, right=False, top=False)

# 軸ラベルの向きを変える
#plt.gca().yaxis.set_tick_params(labelrotation=90)

# 軸の補助目盛表示設定
# plot_ra_floatが時分秒(hms)なので、X軸の補助目盛の間隔は10分(10/60分)
plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(10/60))
# Y軸の補助目盛 1度
plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(1))

# 補助線表示設定
plt.gca().grid(which='major', linestyle='-', linewidth=0.5)
plt.gca().grid(which='minor', linestyle=':', linewidth=0.1)

# 座標をプロット
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.plot.html
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.annotate.html

for (ma, ms, ra, de) in zip(mag_float, mag_float_s, plot_ra_float, plot_dec_float):
    #print(ma)
    #print(ms)
    #print(ra)
    #print(de)
    
    # 視等級に応じてマーカーの形、色(グレースケール)、サイズ を変える
    if round(ma,0) <= 0:
        plt.plot(ra,de, marker='*', color='0.1', markersize=ms)
    elif round(ma,0) == 1:
        plt.plot(ra,de, marker='h', color='0.2', markersize=ms)
    elif round(ma,0) == 2:
        plt.plot(ra,de, marker='p', color='0.3', markersize=ms)
    elif round(ma,0) == 3:
        plt.plot(ra,de, marker='^', color='0.4', markersize=ms)
    elif round(ma,0) == 4:
        plt.plot(ra,de, marker='D', color='0.5', markersize=ms)
    elif round(ma,0) == 5:
        plt.plot(ra,de, marker='s', color='0.6', markersize=ms)
    elif round(ma,0) >= 6:
        plt.plot(ra,de, marker='o', color='0.7', markersize=ms)
        
    # マーカー注釈
    plt.annotate(str(ma) + ')', xy=(ra, de), xytext=(ra-0.025, de-0.01), size=4)


''' ファイル出力処理 '''

# ファイル名
outputFilename = appFilename + '_' + seizaName + '_' + seizaMag + '_' + figSize_mm

# 余白設定 上下左右均等
pad_mm = 0
pad_in = round((float(pad_mm)*0.03937),2)
#print(pad_in)

# ファイル出力 PDF形式
plt.savefig('./' + outputFilename + '.pdf', facecolor='azure', pad_inches=pad_in)
print('Generated ' + outputFilename + '.pdf' + ' !!')

# ファイル出力 PNG形式
#plt.savefig('./' + outputFilename + '.png', facecolor='azure', pad_inches=pad_in)
#print('Generated ' + outputFilename + '.png' + ' !!')

