'''
https://qiita.com/phyblas/items/a801b0f319742245ad2e
pip install astroquery

--------------- ---------
Package         Version
--------------- ---------
astropy         3.2.1
astroquery      0.3.9
beautifulsoup4  4.8.0
certifi         2019.6.16
chardet         3.0.4
cycler          0.10.0
entrypoints     0.3
html5lib        1.0.1
idna            2.8
keyring         19.0.2
kiwisolver      1.1.0
matplotlib      3.1.1
numpy           1.17.0
pip             19.2.2
pyparsing       2.4.2
python-dateutil 2.8.0
pywin32-ctypes  0.2.0
requests        2.22.0
setuptools      41.1.0
six             1.12.0
soupsieve       1.9.2
urllib3         1.25.3
webencodings    0.5.1
wheel           0.33.4
--------------- ---------

'''

import os
import numpy as np
import sys

import astropy.coordinates
from astropy.coordinates import SkyCoord
from astroquery.simbad import Simbad

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick

args = sys.argv
#print(args)        # ['astropy_sample4.py', 'Orion', '3']
#print(args[0])     # astropy_sample4.py
#print(args[1])     # Orion
#print(args[2])     # 3

seizaName=args[1]
seizaMag=args[2]

simbad = Simbad()
simbad.add_votable_fields('flux(V)')
hoshi = simbad.query_criteria('Vmag<=' + seizaMag,otype='star')
#print(hoshi)
#print("hoshi[]の要素数：" + str(len(hoshi)))

sc = SkyCoord(ra=hoshi['RA'],dec=hoshi['DEC'],unit=['hourangle','deg'])
seiza = sc.get_constellation()
#print(seiza)
#print("seiza[]の要素数：" + str(len(seiza)))

ra,dec = sc.ra,sc.dec
#z = (seiza[:,None]==np.unique(seiza)).argmax(1)
#iro = np.stack([z/87,z%5/4,1-z%4/4],1)
s = (5-hoshi['FLUX_V'])*3
o = (seiza==seizaName)

plt.figure(figsize=(8.27, 11.69))   # A4 タテ
#plt.figure(figsize=(11.69, 8.27))  # A4 ヨコ
plt.gca(facecolor='white',aspect=1,title=seizaName + ' (Vmag<=' + seizaMag + ')')
plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(1))
plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(1))
plt.grid(which='both', linestyle = "--", linewidth = 0.5)

plt.scatter(ra[o],dec[o],c='red',s=s[o], marker='*')
plt.savefig('./astropy_sample4_' + seizaName + '.pdf')