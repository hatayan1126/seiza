# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys
import sqlite3

# OpenCV
import cv2


''' 格子描画処理 '''

# 幅方向のグリッド間隔(単位はピクセル)
x_step=10

# 高さ方向のグリッド間隔(単位はピクセル)
y_step=10

# 画像を読み出しオブジェクトimgに代入
img = cv2.imread(outputFilename)

# オブジェクトimgのshapeメソッドの1つ目の戻り値(画像の高さ)をimg_yに
# 2つ目の戻り値(画像の幅)をimg_xに代入
img_y,img_x=img.shape[:2]  

# 横線を引く
# y_stepからimg_yの手前までy_stepおきに白い(BGRすべて255)横線を引く
img[y_step:img_y:y_step, :, :] = 128

# 縦線を引く
# x_stepからimg_xの手前までx_stepおきに白い(BGRすべて255)縦線を引く
img[:, x_step:img_x:x_step, :] = 128

outputFilenameGrid=appFilename + '_' + seizaName + '_grid' + '.' + fileType

cv2.imwrite('./' + outputFilenameGrid, img) #ファイル名'grid.png'でimgを保存
print('Generated ' + outputFilenameGrid + ' !!')

