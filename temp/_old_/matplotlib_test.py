import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot  as plt

data_Xaxis = np.array(range(60))
data_Yaxis = np.sin(data_Xaxis / 5)

plt.plot(data_Yaxis)
plt.savefig('./matplotlib_test.png')