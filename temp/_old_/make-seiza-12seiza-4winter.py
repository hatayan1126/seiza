# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys
import sqlite3

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-12seiza.py', '3', '100']
#print(args[0])     # make-seiza-12seiza
#print(args[1])     # 3～5
#print(args[2])     # 100mm(1mm = 0.03937in)

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename=os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaMag(視等級)」に代入
seizaMag=args[1]
#print(seizaMag)

# 第2引数をmmを変数「figSize_mm」に代入
figSize_mm=args[2]
#print(figSize_mm)

# 第2引数をmmをinに変換して変数「figSize_in」に代入
figSize_in=round((float(figSize_mm)*0.03937),2)
#print(figSize_in)

# 星座名は固定
seizaName = '12_ecliptical_constellations'
#print(seizaName)

''' 星座リスト指定 '''

# 星座リスト
# Ari:おひつじ座  3/21～ 4/19 *
# Tau:おうし座    4/20～ 5/20
# Gem:ふたご座    5/21～ 6/21
# Cnc:かに座      6/22～ 7/22
# Leo:しし座      7/23～ 8/22
# Vir:おとめ座    8/23～ 9/22
# Lib:てんびん座  9/23～10/23
# Sco:さそり座   10/24～11/22
# Sgr:いて座     11/23～12/21
# Cap:やぎ座     12/22～ 1/19
# Aqr:みずがめ座  1/20～ 2/18 *
# Psc:うお座      2/19～ 3/20 *
members = ['Aqr', 'Psc', 'Ari']
#print(members)


''' SQLite操作処理 '''

# SQLiteデータベースに接続
dbname = 'make-seiza-sqlite-hygdatav3.db'
conn = sqlite3.connect(dbname)
cursor = conn.cursor()

# リスト変数 data_array を宣言
data_array = []

for m in members:
    
    # SQLを実行
    sql = 'select hip, proper, mag, ra, dec, dist from hygdata_v3 where con=? AND mag<=?'
    data = (m, seizaMag)
    cursor.execute(sql, data)
    
    # SQLの実行結果をリスト変数 data_array に追加
    for c in cursor:
        data_array.append(c)

#print(data_array)

# sqlite クローズ
conn.close()


''' データ変換処理 '''

# リスト変数 hip, proper, mag, plot_ra, plot_dec, plot_dist を宣言
hip = []       # HipparcosID番号
proper = []    # 恒星名
mag = []       # 見かけの等級
plot_ra = []   # 赤経
plot_dec = []  # 赤緯
plot_dist = [] # 距離(パーセク)

# リスト変数  hip, proper, mag, plot_ra, plot_dec, plot_dist に data_array のデータを追加
for i in data_array:
    hip.append(i[0])
    proper.append(i[1])
    mag.append(i[2])
    plot_ra.append(i[3])
    plot_dec.append(i[4])
    plot_dist.append(i[5])

# リスト mag を float型 に変換
mag_float = [float(s) for s in mag]
#print(mag_float)

# mag_float を マーカーサイズ用 mag_float_s に変換
# 第2引数で指定した視等級を基準に調整
mag_float_s = [round(((float(seizaMag)+3)-(float(s)+2))*1, 2) for s in mag]
#print(mag_float_s)

# 赤経リスト plot_ra を float型 に変換
plot_ra_float = [float(s) for s in plot_ra]
# 時分秒(hms)を度分秒に変換する場合は x15 する
#plot_ra_float = [(float(s)*15) for s in plot_ra]
#print(plot_ra_float)

# 赤経の最大値
plot_ra_max_float = max(plot_ra_float)
#print(plot_ra_max_float)

# 赤経の最小値
plot_ra_min_float = min(plot_ra_float)
#print(plot_ra_min_float)

# 赤経の差＝X軸の範囲
plot_ra_delta_float=plot_ra_max_float - plot_ra_min_float
#print(plot_ra_delta_float)

# 赤緯リスト plot_dec を float型 に変換
plot_dec_float = [float(s) for s in plot_dec]
#print(plot_dec_float)

# 赤緯の最大値
plot_dec_max_float = max(plot_dec_float)
#print(plot_dec_max_float)

# 赤緯の最小値
plot_dec_min_float = min(plot_dec_float)
#print(plot_dec_min_float)

# 赤緯の差＝Y軸の範囲
plot_dec_delta_float=plot_dec_max_float - plot_dec_min_float
#print(plot_dec_delta_float)

# リスト plot_dist を float型 に変換
plot_dist_float = [float(s)*3.262/100 for s in plot_dist]
#print(plot_dist)


''' 星座描画処理 '''

# 画像ザイズ指定
# はがき タテ 100mm(3.94in)×148mm(5.83in)
# pdf出力時はインチ、png出力時はピクセル

# X軸に対するY軸の比率を計算
plot_aspect_float = plot_dec_delta_float/plot_ra_delta_float
#print(plot_aspect_float)

# グラフサイズの横は100mm(3.94in)で固定
# グラフサイズの縦はX軸とY軸の比に応じて可変
plt.figure(figsize=(figSize_in, figSize_in*plot_aspect_float))

# 背景とタイトルの設定
plt.gca(facecolor='white',aspect=1,title=seizaName + ' (Vmag<=' + seizaMag + ') ' + appFilename )

# y軸の範囲を固定
plt.gca().set_ylim(-50, 50)

# グラフの縦横比(アスペクト比)
# plot_ra_floatが時分秒(hms)なので、縦横比をequalにするために 1:1/15(0.066...) に設定
plt.gca().set_aspect(1/15)

# x座標を反転させる
plt.gca().invert_xaxis()

# 軸の目盛ラベル設定
plt.gca().tick_params(which='both', direction='inout', labelsize=4)

# 軸の目盛ラベル非表示
#plt.gca().tick_params(labelbottom=False, labelleft=False, labelright=False, labeltop=False)
# 軸の目盛線非表示
#plt.gca().tick_params(bottom=False, left=False, right=False, top=False)

# 軸ラベルの向きを変える
plt.gca().yaxis.set_tick_params(labelrotation=90)

# 軸の補助目盛表示設定
# plot_ra_floatが時分秒(hms)なので、X軸の補助目盛の間隔は10分(10/60分)
plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(10/60))
# Y軸の補助目盛 1度
plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(1))

# 補助線表示設定
plt.gca().grid(which='major', linestyle = '-', linewidth = 0.5)
plt.gca().grid(which='minor', linestyle = ':', linewidth = 0.1)

# 座標をプロット
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.plot.html
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.annotate.html

for (hi, pr, ma, ms, ra, de, di) in zip(hip, proper, mag_float, mag_float_s, plot_ra_float, plot_dec_float, plot_dist_float):
    #print(hi)
    #print(pr)
    #print(ma)
    #print(ms)
    #print(ra)
    #print(de)
    #print(di)
    
    # 視等級に応じてマーカーの形、色(グレースケール)、サイズ を変える
    if round(ma,0) <= 0:
        plt.plot(ra,de, marker='*', color='0.1', markersize=ms)
    elif round(ma,0) == 1:
        plt.plot(ra,de, marker='h', color='0.2', markersize=ms)
    elif round(ma,0) == 2:
        plt.plot(ra,de, marker='p', color='0.3', markersize=ms)
    elif round(ma,0) == 3:
        plt.plot(ra,de, marker='^', color='0.4', markersize=ms)
    elif round(ma,0) == 4:
        plt.plot(ra,de, marker='D', color='0.5', markersize=ms)
    elif round(ma,0) == 5:
        plt.plot(ra,de, marker='s', color='0.6', markersize=ms)
    elif round(ma,0) >= 6:
        plt.plot(ra,de, marker='o', color='0.7', markersize=ms)
        
    # マーカー注釈
    plt.annotate(pr + '(' + str(ma) + ')', xy=(ra, de), xytext=(ra-0.035, de-0.01), size=2)

''' ファイル出力処理 '''

# ファイル名
outputFilename = appFilename + '_' + seizaName + '_' + seizaMag + '_' + figSize_mm

# ファイル出力 PDF形式
plt.savefig('./' + outputFilename + '.pdf', facecolor='azure', bbox_inches='tight', pad_inches=0.1)
print('Generated ' + outputFilename + '.pdf' + ' !!')

# ファイル出力 PNG形式
#plt.savefig('./' + outputFilename + '.png', facecolor='azure', bbox_inches='tight', pad_inches=0.1)
#print('Generated ' + outputFilename + '.png' + ' !!')

