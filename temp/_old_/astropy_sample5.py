'''
https://qiita.com/phyblas/items/a801b0f319742245ad2e
pip install astroquery

--------------- ---------
Package         Version
--------------- ---------
astropy         3.2.1
astroquery      0.3.9
beautifulsoup4  4.8.0
certifi         2019.6.16
chardet         3.0.4
cycler          0.10.0
entrypoints     0.3
html5lib        1.0.1
idna            2.8
keyring         19.0.2
kiwisolver      1.1.0
matplotlib      3.1.1
numpy           1.17.0
pip             19.2.2
pyparsing       2.4.2
python-dateutil 2.8.0
pywin32-ctypes  0.2.0
requests        2.22.0
setuptools      41.1.0
six             1.12.0
soupsieve       1.9.2
urllib3         1.25.3
webencodings    0.5.1
wheel           0.33.4
--------------- ---------

'''

import os
import os.path
import numpy as np
import sys

import astropy.coordinates
from astropy.coordinates import SkyCoord
from astroquery.simbad import Simbad

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick

args = sys.argv
#print(args)        # ['astropy_sample4.py', 'Orion', '3', 'pdf']
#print(args[0])     # astropy_sample4.py
#print(args[1])     # Orion
#print(args[2])     # 3～5
#print(args[3])     # pdf or png

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename=os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaName(星座名)」に代入
seizaName=args[1]
#print(seizaName)

# 第2引数を変数「seizaMag(視等級)」に代入
seizaMag=args[2]
#print(seizaMag)

# 第3引数を変数「fileType(ファイル形式)」に代入
fileType=args[3]
#print(fileType)

# ---------------------------
# SimbadClass
# https://astroquery.readthedocs.io/en/latest/api/astroquery.simbad.SimbadClass.html
# ---------------------------
#
# クラス「Simbad()」のインスタンス「simbad」を生成
simbad = Simbad()

# 項目に視等級「flux(V)」を追加
simbad.add_votable_fields('flux(V)')

# 変数「hoshi」に simbad.query_criteria の結果を代入
# フィルタ：視等級(Vmag)を第2引数(seizaMag)以下で、タイプは星(star)のみとする
hoshi = simbad.query_criteria('Vmag<=' + seizaMag, otype='star')
#print(hoshi[0])
#print("hoshi[]の要素数：" + str(len(hoshi)))

# ---------------------------
# SkyCoord
# https://docs.astropy.org/en/stable/api/astropy.coordinates.SkyCoord.html
# ---------------------------
#
# SkyCoordで座標データの変換
sc = SkyCoord(ra=hoshi['RA'],dec=hoshi['DEC'],unit=['hourangle','deg'])

# 星座名を取得
seiza = sc.get_constellation()
#print(seiza[0])
#print("seiza[]の要素数：" + str(len(seiza)))

# 赤経(right ascension)
# https://ja.wikipedia.org/wiki/赤経
ra = sc.ra
#print("変数ra：" + str(ra[0]))

# 赤緯(declination)
# https://ja.wikipedia.org/wiki/赤緯
dec = sc.dec
#print("変数dec：" + str(dec[0]))

# 第1引数(seizaName)に一致していれば True、そうでなけれなFalse
o = (seiza==seizaName)
#print("変数o：" + str(o[0]))


# 画像生成処理

# サイズ設定
if fileType=='pdf':
    #print(fileType)
    
    # 視等級をマーカーサイズ(s)用に補正
    # 0等星=250 ～ 5等星=0 ～ 10等星=-250
    s = (5-hoshi['FLUX_V'])*50
    #print("変数s：" + str(s[0]))
    
    # pdf出力時：インチ指定
    plt.figure(figsize=(8.27, 11.69))   # A4タテ
    
elif fileType=='png':
    #print(fileType)
    
    # png出力時：ピクセル指定
    # はがき 低解像度 96dpi 約33KB
    # 視等級をマーカーサイズ(s)用に補正
    # 0等星=50 ～ 5等星=0 ～ 10等星=-50
    s = (5-hoshi['FLUX_V'])*10
    #print("変数s：" + str(s[0]))
    plt.figure(figsize=(378/100, 559/100), dpi=96) # タテ
    #plt.figure(figsize=(559/100, 378/100), dpi=96) # ヨコ
    
    # png出力時：ピクセル指定
    # A4 低解像度 96dpi 約56KB
    # 視等級をマーカーサイズ(s)用に補正
    # 0等星=500 ～ 5等星=0 ～ 10等星=-500
    #s = (5-hoshi['FLUX_V'])*100
    #print("変数s：" + str(s[0]))
    #plt.figure(figsize=(794/100, 1123/100), dpi=96) # タテ
    #plt.figure(figsize=(1123/100, 794/100), dpi=96) # ヨコ
    
    # png出力時：ピクセル指定
    # A4 高解像度 350dpi 約1MB
    # 視等級をマーカーサイズ(s)用に補正
    # 0等星=5,000 ～ 5等星=0 ～ 10等星=-5,000
    #s = (5-hoshi['FLUX_V'])*1000
    #print("変数s：" + str(s[0]))
    #plt.figure(figsize=(2894/100, 4093/100), dpi=350) # タテ
    #plt.figure(figsize=(4093/100, 2894/100), dpi=350) # ヨコ
    
else:
    print('拡張子は pdf か png を指定してください。')
    exit()


# 出力サイズ指定

# 背景とタイトルの設定
plt.gca(facecolor='white',aspect=1,title=seizaName + ' (Vmag<=' + seizaMag + ')')

# x座標を反転させる
plt.gca().invert_xaxis()

# 目盛文字サイズ設定
plt.tick_params(labelsize=6)

# 軸ラベルの向きを変える
#plt.yticks(rotation=90)

# 軸の目盛表示設定
plt.gca().xaxis.set_tick_params(which='both', direction='inout')
plt.gca().yaxis.set_tick_params(which='both', direction='inout')

# 軸の補助目盛表示設定
plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(1))
plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(0.5))

# 補助線表示設定
plt.grid(which='both', linestyle = "--", linewidth = 0.5)


# 座標をプロット
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.scatter.html
# ra[]   赤経
# dec[]  赤緯
# s[]    視等級に応じたマーカーサイズ
# c      マーカーの色
# marker マーカーの種類
plt.scatter(ra[o], dec[o], s=s[o],c='red', marker='*')

# ファイルに出力
outputFilename=appFilename + '_' + seizaName + '.' + fileType
plt.savefig('./' + outputFilename, facecolor="azure", bbox_inches='tight', pad_inches=0.1)
print('Generated ' + outputFilename + ' !!')