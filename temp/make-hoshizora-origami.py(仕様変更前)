# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sqlite3
import sys

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-sqlite.py', 'Ori', '3']
#print(args[0])     # make-seiza-sqlite.py
#print(args[1])     # 星座(略称)
#print(args[2])     # 3～5

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename = os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaName(星座名)」に代入
seizaName = args[1]
#print(seizaName)

# 第2引数を変数「seizaMag(視等級)」に代入
seizaMag = args[2]
#print(seizaMag)


''' 星座データ取得処理 '''

# SQLiteデータベースに接続
dbname = './hygdatav3.db'
conn = sqlite3.connect(dbname)
cursor = conn.cursor()

# 星座を構成する星を格納するリスト変数 seiza_array1, seiza_array2 を宣言
seiza_array1 = []
seiza_array2 = []

# 星座を構成する星を取得するSQLを実行
s_sql1 = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE con=? AND mag<=?'
s_data1 = (seizaName, seizaMag)
s_cursor1 = cursor.execute(s_sql1, s_data1)

# SQLの実行結果をリスト変数 seiza_array に追加
for s in s_cursor1:
    seiza_array1.append(s)
    
# レコードカウンター
seiza_count = len(seiza_array1)
print('seiza_count='+str(seiza_count))

# 赤経 0時/24時 をまたいでいるか判定
# 赤経の最大と最小の差が12を超えている場合は 0時/24時 をまたいでいると判定

# 赤経 0時/24時 をまたぐフラグ plot_ra_24cross を false で初期化
plot_ra_24cross = 'false'

s_sql2 = 'SELECT max(ra)-min(ra) FROM hygdata_v3 WHERE con=? AND mag<=?'
s_data2 = (seizaName, seizaMag)
s_cursor2 = cursor.execute(s_sql2, s_data2)

for s2 in s_cursor2:
    seiza_array2.append(s2)
#print('s2='+str(s2))

# 赤経 0時/24時 をまたいでいれば plot_ra_24cross を true にする
if float(s2[0]) > 12:
    plot_ra_24cross = 'true'
    print('この星座は 0時/24時 をまたいでいます')

# sqlite クローズ
conn.close()


''' 星座データ変換処理 '''

# リスト変数 s_hip, s_proper, s_mag, s_plot_ra1, s_plot_ra2, s_plot_dec を宣言
s_hip = []       # HipparcosID番号
s_proper = []    # 恒星名
s_mag = []       # 視等級
s_plot_ra1 = []  # 赤経
s_plot_ra2 = []  # 赤経(0時/24時をまたいでいる場合のリスト)
s_plot_dec = []  # 赤緯

# リスト変数  s_hip, s_proper, s_mag, s_plot_ra1, s_plot_ra2, s_plot_dec に seiza_array1 のデータを追加
for i in seiza_array1:
    s_hip.append(i[0])
    s_proper.append(i[1])
    s_mag.append(i[2])
    s_plot_ra1.append(i[3])
    s_plot_ra2.append(i[3])
    s_plot_dec.append(i[4])
    
#print('s_hip='+str(s_hip))
#print('s_proper='+str(s_proper))
#print('s_mag='+str(s_mag))
#print('s_plot_ra1='+str(s_plot_ra1))
#print('s_plot_ra2='+str(s_plot_ra2))
#print('s_plot_dec='+str(s_plot_dec))

# リスト mag を float型 に変換
s_mag_float = [float(s) for s in s_mag]
#print(h_mag_float)

# 赤経 0時/24時 をまたいでいる場合の処理
# s_plot_ra2 のなかから 12 より小さい座標に 24 を足し s_plot_ra1 に格納し直す
if plot_ra_24cross == 'true':
    s_plot_ra1 = []
    for s_ra2 in s_plot_ra2:
        if float(s_ra2) < 12:
            s_plot_ra1.append(float(s_ra2)+24)
        else:
            s_plot_ra1.append(float(s_ra2))

# 赤経リスト s_plot_ra1 を float型 に変換
s_plot_ra_float = [float(s) for s in s_plot_ra1]
# 時分秒(hms)を度分秒に変換する場合は x15 する
#s_plot_ra_float = [(float(s)*15) for s in s_plot_ra1]
#print('s_plot_ra_float='+str(s_plot_ra_float))

# 赤経最小値を変数「s_plot_ra_min」に代入
s_plot_ra_min = min(s_plot_ra_float)
#print('s_plot_ra_min='+str(s_plot_ra_min))

# 赤経最大値を変数「s_plot_ra_max」に代入
s_plot_ra_max = max(s_plot_ra_float)
#print('s_plot_ra_max='+str(s_plot_ra_max))

# 赤経範囲を変数「s_plot_ra_delta」に代入
s_plot_ra_delta = s_plot_ra_max-s_plot_ra_min
#print('s_plot_ra_delta='+str(s_plot_ra_delta))

# 赤経の中心を取得
s_plot_ra_center = s_plot_ra_min+(s_plot_ra_delta/2)
#print('s_plot_ra_center='+str(s_plot_ra_center))

# 赤経パッディングを変数「s_plot_dec_pad」に代入
s_plot_ra_pad = s_plot_ra_delta*0.3
#print('s_plot_ra_pad='+str(s_plot_ra_pad))

# 赤緯リスト plot_dec を float型 に変換
s_plot_dec_float = [float(s) for s in s_plot_dec]
#print('s_plot_dec_float='+str(s_plot_dec_float))

# 赤緯最小値を変数「s_plot_dec_min」に代入
s_plot_dec_min = min(s_plot_dec_float)
#print('s_plot_dec_min='+str(s_plot_dec_min))

# 赤緯最大値を変数「s_plot_dec_max」に代入
s_plot_dec_max = max(s_plot_dec_float)
#print('s_plot_dec_max='+str(s_plot_dec_max))

# 赤緯範囲を変数「s_plot_dec_delta」に代入
s_plot_dec_delta = s_plot_dec_max-s_plot_dec_min
#print('s_plot_dec_delta='+str(s_plot_dec_delta))

# 赤緯の中心を取得
s_plot_dec_center = s_plot_dec_min+(s_plot_dec_delta/2)
#print('s_plot_dec_center='+str(s_plot_dec_center))

# 赤緯パッディングを変数「s_plot_dec_pad」に代入
s_plot_dec_pad = s_plot_dec_delta*0.3
#print('s_plot_dec_pad='+str(s_plot_dec_pad))


# 赤経と赤緯の長さをチェックして長い方に合わせる
if s_plot_ra_delta*15 > s_plot_dec_delta:
    #print('s_plot_ra_deltaが長い')
    s_plot_ra_min = s_plot_ra_min-(s_plot_dec_pad/15)
    s_plot_ra_max = s_plot_ra_max+(s_plot_dec_pad/15)
    s_plot_dec_min = s_plot_dec_center-((s_plot_ra_delta*15)/2)-s_plot_dec_pad
    s_plot_dec_max = s_plot_dec_center+((s_plot_ra_delta*15)/2)+s_plot_dec_pad
elif s_plot_ra_delta*15 < s_plot_dec_delta:
    #print('s_plot_dec_deltaが長い')
    s_plot_ra_min = s_plot_ra_center-((s_plot_dec_delta/15)/2)-s_plot_ra_pad
    s_plot_ra_max = s_plot_ra_center+((s_plot_dec_delta/15)/2)+s_plot_ra_pad
    s_plot_dec_min = s_plot_dec_min-(s_plot_ra_pad*15)
    s_plot_dec_max = s_plot_dec_max+(s_plot_ra_pad*15)

#print('s_plot_ra_min='+str(s_plot_ra_min))
#print('s_plot_ra_max='+str(s_plot_ra_max))
#print('s_plot_dec_min='+str(s_plot_dec_min))
#print('s_plot_dec_max='+str(s_plot_dec_max))
#print('s_plot_ra_delta='+str((s_plot_ra_max-s_plot_ra_min)*15))
#print('s_plot_dec_delta='+str(s_plot_dec_max-s_plot_dec_min))


''' 恒星データ取得処理 '''

# SQLiteデータベースに接続
dbname = './hygdatav3.db'
conn = sqlite3.connect(dbname)
cursor = conn.cursor()

# 指定範囲にある星をリスト変数 hoshi_array を宣言
hoshi_array = []

if plot_ra_24cross == 'true':
    # 0時側の星座を取得するSQLを実行
    h_sql1  = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE mag<=? AND (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?)'
    h_data1 = (6, 0, s_plot_ra_max-24+s_plot_ra_pad, s_plot_dec_min-s_plot_dec_pad, s_plot_dec_max+s_plot_dec_pad)
    h_cursor1 = cursor.execute(h_sql1, h_data1)
    
    # SQLの実行結果をリスト変数 seiza_array に追加
    for h1 in h_cursor1:
        hoshi_array.append(h1)
        
    # 24時側の星座を取得するSQLを実行
    h_sql2  = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE mag<=? AND (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?)'
    h_data2 = (6, s_plot_ra_min-s_plot_ra_pad, 24, s_plot_dec_min-s_plot_dec_pad, s_plot_dec_max+s_plot_dec_pad)
    h_cursor2 = cursor.execute(h_sql2, h_data2)
    
    # SQLの実行結果をリスト変数 seiza_array に追加
    for h2 in h_cursor2:
        hoshi_array.append(h2)
else:
    # SQLを実行
    h_sql1 = 'SELECT hip, proper, mag, ra, dec FROM hygdata_v3 WHERE mag<=? AND (ra BETWEEN ? AND ?) AND (dec BETWEEN ? AND ?)'
    h_data1 = (6, s_plot_ra_min-s_plot_ra_pad, s_plot_ra_max+s_plot_ra_pad, s_plot_dec_min-s_plot_dec_pad, s_plot_dec_max+s_plot_dec_pad)
    h_cursor1 = cursor.execute(h_sql1, h_data1)
    
    # SQLの実行結果をリスト変数 seiza_array に追加
    for h in h_cursor1:
        hoshi_array.append(h)


# レコードカウンター
hoshi_count = len(hoshi_array)
print('hoshi_count=' + str(hoshi_count))

# sqlite クローズ
conn.close()

# リスト変数 h_hip, h_proper, h_mag, h_plot_ra1, h_plot_ra2, h_plot_dec を宣言
h_hip = []       # HipparcosID番号
h_proper = []    # 恒星名
h_mag = []       # 視等級
h_plot_ra1 = []  # 赤経
h_plot_ra2 = []  # 赤経(0時/24時をまたいでいる場合のリスト)
h_plot_dec = []  # 赤緯

# リスト変数  h_hip, h_proper, h_mag, h_plot_ra1, h_plot_ra2, h_plot_dec に hoshi_array のデータを追加
for i in hoshi_array:
    h_hip.append(i[0])
    h_proper.append(i[1])
    h_mag.append(i[2])
    h_plot_ra1.append(i[3])
    h_plot_ra2.append(i[3])
    h_plot_dec.append(i[4])

#print('h_hip='+str(h_hip))
#print('h_proper='+str(h_proper))
#print('h_mag='+str(h_mag))
#print('h_plot_ra1='+str(h_plot_ra1))
#print('h_plot_ra2='+str(h_plot_ra2))
#print('h_plot_dec='+str(h_plot_dec))


# リスト mag を float型 に変換
h_mag_float = [float(s) for s in h_mag]
#print(h_mag_float)

# 赤経 0時/24時 をまたいでいる場合の処理
# h_plot_ra2 のなかから 12 より小さい座標に 24 を足し h_plot_ra1 に格納し直す
if plot_ra_24cross == 'true':
    h_plot_ra1 = []
    for h_ra2 in h_plot_ra2:
        if float(h_ra2) < 12:
            h_plot_ra1.append(float(h_ra2)+24)
        else:
            h_plot_ra1.append(float(h_ra2))

# 赤経リスト h_plot_ra を float型 に変換
h_plot_ra_float = [float(s) for s in h_plot_ra1]
# 時分秒(hms)を度分秒に変換する場合は x15 する
#h_plot_ra_float = [(float(s)*15) for s in h_plot_ra1]
#print(h_plot_ra_float)

# 赤緯リスト h_plot_dec を float型 に変換
h_plot_dec_float = [float(s) for s in h_plot_dec]
#print(h_plot_dec_float)


''' 星座描画処理 '''

# 画像ザイズ指定
# 横サイズを変数「figX_mm」に代入
figX_mm = 150*1.1
#print(figX_mm)

# 縦サイズを変数「figY_mm」に代入
figY_mm = 150*1.1
#print(figY_mm)

# 横サイズをinに変換して変数「figX_in」に代入
figX_in = round(float(figX_mm)*0.03937, 2)
#print(figX_in)

# 縦サイズをinに変換して変数「figX_in」に代入
figY_in = round(float(figY_mm)*0.03937, 2)
#print(figY_in)

# 画像サイズ指定
plt.figure(figsize=(figX_in, figY_in))

# グラフの外枠を非表示
#plt.axis('off')

# 背景とタイトルの設定
plt.gca(facecolor='#002255', title= appFilename + ' / ' + seizaName + '(' + seizaMag + ') / ' + str(hoshi_count) + ' stars')

# グラフの縦横比(アスペクト比)
# plot_ra_floatが時分秒(hms)なので、縦横比をequalにするために 1:1/15(0.066...) に設定
plt.gca().set_aspect(1/15)

# x座標を反転させる
plt.gca().invert_xaxis()

# 軸の目盛ラベル設定
#plt.gca().tick_params(which='both', direction='inout', labelsize=4)

# 軸の目盛ラベル非表示
plt.gca().tick_params(labelbottom=False, labelleft=False, labelright=False, labeltop=False)

# 軸の目盛線非表示
plt.gca().tick_params(bottom=False, left=False, right=False, top=False)

# 軸ラベルの向きを変える
#plt.gca().yaxis.set_tick_params(labelrotation=90)

# 軸の補助目盛表示設定
# plot_ra_floatが時分秒(hms)なので、X軸の補助目盛の間隔は10分(10/60分)
#plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(10/60))
# Y軸の補助目盛 1度
#plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(5))

# 補助線表示設定
#plt.gca().grid(which='major', linestyle='-', linewidth=0.1)
#plt.gca().grid(which='minor', linestyle=':', linewidth=0.05)

# 星空をプロット
for (h_hi, h_pr, h_ma, h_ra, h_de) in zip(h_hip, h_proper, h_mag_float, h_plot_ra_float, h_plot_dec_float):
    #print(h_hi)
    #print(h_pr)
    #print(h_ma)
    #print(h_ra)
    #print(h_de)
    # 視等級に応じてマーカーの形、色、サイズ を変える
    if round(h_ma, 0) <= 0:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=1.0)
    elif round(h_ma, 0) == 1:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=0.8)
    elif round(h_ma, 0) == 2:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=0.6)
    elif round(h_ma, 0) == 3:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=0.4)
    elif round(h_ma, 0) == 4:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=0.2)
    elif round(h_ma, 0) == 5:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=0.1)
    elif round(h_ma, 0) >= 6:
        plt.plot(h_ra, h_de, marker=',', color='white', markersize=0.1)
    
    # マーカー注釈
    plt.annotate(h_pr, xy=(h_ra, h_de), xytext=(h_ra-0.025, h_de-0.01), color='white', size=4)
    
    
# 星座をプロット
for (s_hi, s_pr, s_ma, s_ra, s_de) in zip(s_hip, s_proper, s_mag_float, s_plot_ra_float, s_plot_dec_float):
    #print(s_hi)
    #print(s_pr)
    #print(s_ma)
    #print(s_ra)
    #print(s_de)
    # 視等級に応じてマーカーの形、色、サイズ を変える
    if round(s_ma, 0) <= 0:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=2.0)
    elif round(s_ma, 0) == 1:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=1.8)
    elif round(s_ma, 0) == 2:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=1.6)
    elif round(s_ma, 0) == 3:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=1.4)
    elif round(s_ma, 0) == 4:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=1.2)
    elif round(s_ma, 0) == 5:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=1.0)
    elif round(s_ma, 0) >= 6:
        plt.plot(s_ra, s_de, marker='D', color='yellow', markersize=0.8)

    # マーカー注釈
    #plt.annotate(s_pr, xy=(s_ra, s_de), xytext=(s_ra-0.025, s_de-0.01), color='white', size=4)


''' ファイル出力処理 '''

# ファイル名
outputFilename = appFilename + '_' + seizaName + '(' + seizaMag + ')'

# 余白の調整
plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95)

# ファイル出力 PDF形式
#plt.savefig('./' + outputFilename + '.pdf', facecolor='azure')
plt.savefig('./' + outputFilename + '.pdf')
print('Generated ' + outputFilename + '.pdf' + ' !! ' + str(hoshi_count) + ' stars')

# ファイル出力 PNG形式
#plt.savefig('./' + outputFilename + '.png', facecolor='azure')
#print('Generated ' + outputFilename + '.png' + ' !! ' + str(hoshi_count) + ' stars')

# ファイル出力 SVG形式
plt.savefig('./' + outputFilename + '.svg', facecolor='azure')
print('Generated ' + outputFilename + '.svg' + ' !! ' + str(hoshi_count) + ' stars')

