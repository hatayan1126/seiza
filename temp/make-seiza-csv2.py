# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys

# pandas
# https://pandas.pydata.org/
# pip install pandas
import pandas as pd

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick


''' 前処理 '''

# 引数を変数args[]に代入
args = sys.argv
#print(args)        # ['make-seiza-csv.py', 'Ori', '3', 'pdf']
#print(args[0])     # make-seiza-csv.py
#print(args[1])     # Ori
#print(args[2])     # 3～5

# 拡張子を省略したファイル名を変数「appFilename(アプリファイル名)」に代入
# https://docs.python.org/ja/3/library/os.path.html#os.path.splitext
appFilename=os.path.splitext(args[0])[0]
#print(appFilename)

# 第1引数を変数「seizaName(星座名)」に代入
seizaName=args[1]
#print(seizaName)

# 第2引数を変数「seizaMag(視等級)」に代入
seizaMag=args[2]
#print(seizaMag)


''' CSVファイル操作処理 '''

# CSVファイルを読み込む
df = pd.read_csv('./make-seiza-hygdata_v3.csv')
#print(df)

# データを抽出
# con==seizaName かつ mag<=seizaMag
df_filtered = df[(df['con'] == seizaName) & (df['mag'] <= int(seizaMag))]
#print(df_filtered)

# 抽出したデータ(df_filtered)の NaN(値なし) を '' に置換
df_fillna = df_filtered.fillna('')
#print(df_fillna)

# DataFrame から List への変換
data_array = df_fillna.values.tolist()
#print(data_array)


''' データ変換処理 '''

# リスト変数 hip, proper, mag, plot_ra, plot_dec を宣言
hip = []       # HipparcosID番号
proper = []    # 恒星名
mag = []       # 見かけの等級
plot_ra = []   # 赤経
plot_dec = []  # 赤緯

# リスト変数 hip, mag, plot_ra, plot_dec に data_array のデータを追加
for i in data_array:
    hip.append(i[1])
    proper.append(i[6])
    mag.append(i[13])
    plot_ra.append(i[7])
    plot_dec.append(i[8])
#print(hip)
#print(proper)
#print(mag)
#print(plot_ra)
#print(plot_dec

# リスト mag を float型 に変換
mag_float = [float(s) for s in mag]
#print(mag_float)

# mag_float を マーカーサイズ用 mag_float_s に変換
# 第2引数で指定した視等級を基準に調整
mag_float_s = [round(float(seizaMag)-float(s)) for s in mag]
#print(mag_float_s)

# リスト plot_ra を float型 に変換
# 24時間を360度に変換するため x15 する
plot_ra_float = [(float(s)*15) for s in plot_ra]
#print(plot_ra_float)

# リスト plot_dec を float型 に変換
plot_dec_float = [float(s) for s in plot_dec]
#print(plot_dec_float)


''' 星座描画処理 '''

# 画像ザイズ指定
# pdf出力時：インチ指定
plt.figure(figsize=(3.94, 5.83)) # はがき タテ

# 背景とタイトルの設定
plt.gca(facecolor='white', title=seizaName + ' (Vmag<=' + seizaMag + ')')

# グラフの縦横比(アスペクト比)
plt.gca().set_aspect('equal')

# x座標を反転させる
plt.gca().invert_xaxis()

# 軸の目盛ラベル設定
plt.gca().tick_params(labelsize=5, which='both', direction='inout')

# 軸ラベルの向きを変える
plt.gca().yaxis.set_tick_params(labelrotation=90)

# 軸の補助目盛表示設定
plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(0.5))
plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(1))

# 補助線表示設定
plt.gca().grid(which='both', linestyle = ':', linewidth = 0.5)

# 座標をプロット
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.plot.html
# https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.annotate.html

for (hi, pr, ma, ms, ra, de) in zip(hip, proper, mag_float, mag_float_s, plot_ra_float, plot_dec_float):
    #print(hi)
    #print(pr)
    #print(ma)
    #print(ms)
    #print(ra)
    #print(de)
    plt.plot(ra, de, marker='o', color='blue', markersize=ms)
    plt.annotate(str(pr) + '(' + str(ma) + ')', xy=(ra, de), xytext=(ra-0.35, de-0.1), size=3)

# ファイルに出力

# ファイル名設定
outputFilename=appFilename + '_' + seizaName + '(' + seizaMag + ').pdf'
#print(outputFilename)

# ファイル出力
plt.savefig('./' + outputFilename, facecolor="azure", bbox_inches='tight', pad_inches=0.1)

# 終了メッセージ
print('Generated ' + outputFilename + ' !!')
