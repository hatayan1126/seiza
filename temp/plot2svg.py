# Standard Library
# https://docs.python.org/ja/3/library/index.html
import os
import os.path
import sys

# NumPy
# https://www.numpy.org/
# pip install numpy
import numpy as np

# Matplotlib
# https://matplotlib.org/
# pip install matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot  as plt
import matplotlib.ticker as tick

''' 描画処理 '''

# 画像ザイズ指定
# 横サイズを変数「figX_mm」に代入
figX_mm = 150*1.1
#print(figX_mm)

# 縦サイズを変数「figY_mm」に代入
figY_mm = 150*1.1
#print(figY_mm)

# 横サイズをinに変換して変数「figX_in」に代入
figX_in = round(float(figX_mm)*0.03937, 2)
#print(figX_in)

# 縦サイズをinに変換して変数「figX_in」に代入
figY_in = round(float(figY_mm)*0.03937, 2)
#print(figY_in)

# 画像サイズ指定
plt.figure(figsize=(figX_in, figY_in))

# グラフの外枠を非表示
#plt.axis('off')

# マーカープロット
zx = range(5)
zy = range(3)
zm = [',', '.', 'o']
zs = range(10, 30, 5)

for (x, s) in zip(zx, zs):
    for (y, m) in zip(zy, zm):
        print('x=' + str(x) + ' | y=' + str(y) + ' | marker=' + str(m) + ' | size=' + str(s))
        plt.plot(x, y, marker=m, color='blue', markersize=s) # point



''' ファイル出力処理 '''

# ファイル名
outputFilename = 'plot2svg'

# 余白の調整
plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95)

# ファイル出力 SVG形式
plt.savefig('./' + outputFilename + '.svg', facecolor='azure')
print('Generated ' + outputFilename + '.svg' + ' !! ')

# ファイル出力 PNG形式
plt.savefig('./' + outputFilename + '.png', facecolor='azure')
print('Generated ' + outputFilename + '.png' + ' !!')

