# 恒星の座標データから星座を描画する  


## 開発環境  

- Windows10(64bit)  
- Python 3.8.0  
    - matplotlib 3.1.2  
    - numpy 1.17.4  
    - astropy 3.2.3  
    - astroquery 0.3.10  
- SQlite3
    - sqlite-tool 3.29.0  
    - sqlite-dll 3.29.0  
- Visual Studio Code 1.41.1  
- EmEditor 19.5.0  
- Inkscape 0.92.4(SVGデータ編集用)  


## ディレクトリ構成  

- make-seiza/  
開発中のアプリの保存場所です。  

- temp/  
試作段階のアプリ、開発終了のアプリの保存場所です。  

- 射手座ロゴマーク/  
プロジェクトのロゴです。  

- 星座画像(iau版)/  
[国際天文学連合 IAU](https://www.iau.org/) の [The Constellations ](https://www.iau.org/public/themes/constellations/) からダウンロードした星座に関するデータの保存場所です。  


## アプリ概要

指定された「星座名」「視等級(見かけの等級)」に応じて恒星の座標データ(*)をもとにプロットし「指定されたサイズ」で出力するアプリです。  
プロットされるのは指定された星座を構成する恒星と、その周辺の恒星です。

サイズ限定  
- はがきサイズ限定：make-seiza/make-hosizora-hagaki.py  
- 折り紙サイズ限定：make-seiza/make-hosizora-origami.py  
  
星座限定  
- 夏の大三角：make-seiza/make-hoshizora-summer-tryangle.py  
- 冬の大三角：make-seiza/make-hoshizora-winter-tryangle.py  
- 冬のダイヤモンド：make-seiza/make-hoshizora-winter-diamond.py  

(*)恒星の座標データ  
[HYG 3.0](http://www.astronexus.com/files/downloads/hygdata_v3.csv.gz) のデータをSQLiteに格納して利用します。  


### make-seiza/make-hosizora-hagaki.py  

指定された「星座名」と「視等級」の恒星をはがきサイズ(148mm x 100mm)で出力する  

```
使い方

> python3 make-hosizora-hagaki.py 星座名(略称) 視等級

```

```
使用例：オリオン座で4等級以上の恒星を描画する

> python3 make-hosizora-hagaki.py Ori 4

```

### make-seiza/make-hosizora-origami.py  

指定された「星座名」と「視等級」の恒星を折り紙サイズ(150mm x 150mm)で出力する  

```
使い方

> python3 make-hosizora-origami.py 星座名(略称) 視等級

```

```
使用例：オリオン座で4等級以上の恒星を描画する

> python3 make-hosizora-origami.py Ori 4

```

### make-seiza/make-hoshizora-summer-tryangle.py    

夏の大三角(はくちょう座、こと座、わし座)の恒星を指定された「視等級」と「サイズ」で出力する  

```
使い方

> python3 make-hoshizora-summer-tryangle.py 視等級 サイズ

```

```
使用例：4等級以上の恒星を200mm x 200mm で描画する

> python3 make-hoshizora-summer-tryangle.py 4 200

```

### make-seiza/make-hoshizora-winter-tryangle.py  

冬の大三角(オリオン座、こいぬ座、おおいぬ座)の恒星を指定された「視等級」と「サイズ」で出力する  

```
使い方

> python3 make-hoshizora-winter-tryangle.py 視等級 サイズ

```

```
使用例：4等級以上の恒星を200mm x 200mm で描画する

> python3 make-hoshizora-winter-tryangle.py 4 200

```

### make-seiza/make-hoshizora-winter-diamond.py  

冬のダイヤモンド(オリオン座、こいぬ座、おおいぬ座、ぎょしゃ座、ふたご座、おうし座)の恒星を指定された「視等級」と「サイズ」で出力する  

```
使い方

> python3 make-hoshizora-winter-diamond.py 視等級 サイズ

```

```
使用例：4等級以上の恒星を200mm x 200mm で描画する

> python3 make-hoshizora-winter-diamond.py 4 200

```

## 課題
- 共通する機能を関数に分割したい  


## 参考文献

- [matplotlib](https://matplotlib.org/)
- [気象データ解析のためのmatplotlibの使い⽅](http://ebcrpa.jamstec.go.jp/~yyousuke/matplotlib/matplotlib.html)
- [astropyを使って星座をいじってみたり](https://qiita.com/phyblas/items/a801b0f319742245ad2e)  
- [astropy](https://www.astropy.org/)  

 